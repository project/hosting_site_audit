<?php

/**
 * @file The SiteAuditProbe class.
 */

class SiteAuditProbe extends SiteReportProbe {

  use DrushSiteAuditCommand, SiteAuditAnalysis;

  const DAILY = 86400;

  protected $enabled = FALSE;

  function __construct($name) {
    parent::__construct($name);
    require_once(dirname(__FILE__) . '/../drush/probes_site_audit.drush.inc');
    require_once($this->getAbstractCheckPath());
    $checks = $this->getAuditChecks();
    require_once($checks[$name]);
    $class = 'SiteAuditCheck' . $name;
    $check = new $class(array());
    $this->check = $check;
    $this->label = $check->getLabel();
    $this->description = $check->getDescription();
    $this->sections = array(basename(dirname($checks[$name])));
  }

  function getAuditChecks() {
    return probes_site_audit_get_checks();
  }

  function getAbstractCheckPath() {
    $check = probes_site_audit_get_checks(TRUE);
    return $check['CheckAbstract'];
  }


}

trait SiteAuditAnalysis {

  public function getAnalysis() {
    return array(
      SiteAuditCheckAbstract::AUDIT_CHECK_SCORE_PASS => array(
        '#status' => self::RESULT_STATUS_OK,
        '#result_callback' => 'getResultPass',
      ),
      SiteAuditCheckAbstract::AUDIT_CHECK_SCORE_WARN => array(
        '#status' => self::RESULT_STATUS_WARNING,
        '#result_callback' => 'getResultWarn',
      ),
      SiteAuditCheckAbstract::AUDIT_CHECK_SCORE_FAIL => array(
        '#status' => self::RESULT_STATUS_ERROR,
        '#result_callback' => 'getResultFail',
      ),
    );
  }

  public function analyse($result) {
    $original = $result;
    $result = @unserialize($result);
    if ($result === FALSE && $original !== 'b:0;') {
      // Use the original value, if it wasn't serialized.
      $result = $original;
    }
    $this->check->registry = $result;
    if (is_array($result)) {
      return array(
        '#status' => self::RESULT_STATUS_INFO,
        '#message' => $this->check->getResultInfo(),
      );
    }
    $analysis = $this->getAnalysis();
    if (array_key_exists($result, $analysis)) {
      $return = $analysis[$result];
      $callback = $analysis[$result]['#result_callback'];
      $return['#message'] = $this->check->$callback();
      return $return;
    }
    else {
      return array(
        '#message' => t('Unknown'),
        '#status' => RESULT_STATUS_UNKNOWN,
      );
    }
  }

}

trait DrushSiteAuditCommand {
  /**
   * Callback to format a Drush site audit check probe.
   */
  function getCommand($alias) {
    // TODO: Investigate the security implications of allowing this.
    return drush_find_drush() . ' ' . $alias . ' probes-site-audit-single-check ' . $this->name;
  }

}

