<?php


/**
 * @file
 * The hosting feature definition for Site Audit module.
 */

/**
 * Register a hosting feature with Aegir.
 *
 * This will be used to generate the 'admin/hosting' page.
 *
 * @return array
 *   associative array indexed by feature key.
 */
function hosting_site_audit_hosting_feature() {
  $features['site_audit'] = array(
    'title' => t('Site Audit Reporting'),
    'description' => t('Probes to audit the health of sites hosted in Aegir.'),
    'status' => HOSTING_FEATURE_DISABLED,
    'module' => 'hosting_site_audit',
    'group' => 'experimental',
  );
  return $features;
}
