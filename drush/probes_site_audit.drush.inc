<?php

/**
 * Implements hook_drush_command().
 */
function probes_site_audit_drush_command() {
  $items = array();

  $items['probes-site-audit-list-checks'] = array(
    'description' => dt('List the checks provided by the Site Audit extention.'),
    'aliases' => array('psalc'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'options' => array(
      'drupal_core_version' => dt('The version of core for which to load checks. Default to "7".'),
    ),
  );

  $items['probes-site-audit-single-check'] = array(
    'description' => dt('Run a single site audit check.'),
    'aliases' => array('psasc'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'arguments' => array(
      'check' => dt('The name of the check to run.'),
    ),
    'required_arguments' => TRUE,
  );

  return $items;
}

/**
 * Command callback for the 'probes-site-audit-single-check' command.
 */
function drush_probes_site_audit_single_check($check) {
  $checks = probes_site_audit_get_checks();
  if (!array_key_exists($check, $checks)) {
    return drush_log(dt('No site audit check found matching `!check`', array('!check' => $check)), 'error');
  }
  $abstract = probes_site_audit_get_checks(TRUE);
  require_once($abstract['CheckAbstract']);
  require_once($checks[$check]);
  $class = 'SiteAuditCheck' . $check;
  $check = new $class(array());
  $score = $check->getScore();
  if ($score == SiteAuditCheckAbstract::AUDIT_CHECK_SCORE_INFO) {
    drush_print(serialize($check->getRegistry()));
  }
  else {
    drush_print($score);
  }
}

/**
 * Command callback for the 'probes-site-audit-list-checks' command.
 */
function drush_probes_site_audit_list_checks() {
  $checks = probes_site_audit_get_checks();
  drush_print(dt("Available site audit checks:\n !checks", array('!checks' => implode(', ', array_flip($checks)))));
}

/**
 * Get the path to the proper site_audit repo.
 */
function probes_site_audit_get_checks_path() {
  return realpath(dirname(__FILE__) . '/../lib/site_audit_7/Check/');
}

/**
 * Scan the filesystem for site audit checks.
 */
function probes_site_audit_scan_for_checks() {
  $files = &drupal_static(__FUNCTION__);
  if (!isset($files)) {
    $path = probes_site_audit_get_checks_path();
    $scan_func = function_exists('drush_log') ? 'drush_scan_directory' : 'file_scan_directory';
    $files = $scan_func($path, '/^.+\.php$/i');
  }
  return $files; 
}

/**
 * Return a list of site_audit checks and their respective filesystem paths.
 */
function probes_site_audit_get_checks($abstract = FALSE) {
  $files = probes_site_audit_scan_for_checks();
  $checks = array();
  foreach ($files as $filepath => $file) {
    $checks[basename(dirname($filepath)) . $file->name] = $filepath;
  }
  if ($abstract) {
    return array('CheckAbstract' => $checks['CheckAbstract']);
  }
  else {
    unset($checks['CheckAbstract']);
  }
  return $checks;
}

/**
 * Shims to allow running Drush code fro the front-end.
 */

if (!function_exists('dt')) {
  function dt($string, $args = array()) {
    return t($string, $args);
  }
}

if (!function_exists('drush_get_option')) {
  function drush_get_option() {}
}
